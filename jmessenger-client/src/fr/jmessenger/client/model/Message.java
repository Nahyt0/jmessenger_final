package fr.jmessenger.client.model;

public class Message {

	private int sender;
	private String content;
	private int room;
	
	public Message(int sender, String content, int room) {
		this.sender = sender;
		this.content = content;
		this.room = room;
	}
	
	public int getSender() {
		return sender;
	}
	
	public void setSender(int sender) {
		this.sender = sender;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}

	public int getRoom() {
		return room;
	}

	public void setRoom(int room) {
		this.room = room;
	}
	
}