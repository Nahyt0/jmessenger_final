package fr.jmessenger.client.model;

public class RoomElement {
	
	private int roomID;
	private String roomName;
	private String content;
	private int lastMessageID;
	
	public RoomElement(int roomID, String roomName) {
		this.roomID = roomID;
		this.roomName = roomName;
		this.content = "";
		this.lastMessageID = 0;
	}
	
	@Override
	public String toString() {
		return roomName;
	}
	
	public void appendContent(String content) {
		this.content += content;
	}
	
	public int getRoomID() {
		return roomID;
	}
	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public int getLastMessageID() {
		return lastMessageID;
	}

	public void setLastMessageID(int lastMessageID) {
		this.lastMessageID = lastMessageID;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}
