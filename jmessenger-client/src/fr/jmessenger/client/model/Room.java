package fr.jmessenger.client.model;

public class Room {
	
	private String nom;
	private String createur;
	
	public Room(String nom, String createur) {
		this.nom = nom;
		this.createur = createur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCreateur() {
		return createur;
	}

	public void setCreateur(String createur) {
		this.createur = createur;
	}
	
}
