package fr.jmessenger.client.model;

public class AddFriend {
	
	private String pseudo;
	
	public AddFriend(String pseudo) {
		this.setPseudo(pseudo);
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	
}
