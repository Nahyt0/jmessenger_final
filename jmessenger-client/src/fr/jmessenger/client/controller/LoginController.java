package fr.jmessenger.client.controller;

import java.io.IOException;

import java.net.URL;

import java.util.ResourceBundle;

import fr.jmessenger.client.MainClass;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class LoginController implements Initializable {
	
	private RequestHandler requestHandler = new RequestHandler();
	
    @FXML
    private TextField tfName;

    @FXML
    private TextField tfFirstName;

    @FXML
    private TextField tfEmail;

    @FXML
    private TextField tfUsername;

    @FXML
    private PasswordField pfPassword;

    @FXML
    private Button btnSignup;

    @FXML
    private Hyperlink hlLogin;
    
    @FXML
    private Button btnLogin;

    @FXML
    private Hyperlink hlSignup;

	@FXML
	private VBox vbox;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
	
	// Use setOnCloseRequest
	
	private void gotoView(ActionEvent event, String fxmlPath) {
		Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource(fxmlPath));
			Node node = (Node) event.getSource();
		    Stage stage = (Stage) node.getScene().getWindow();
	        Scene scene=new Scene(root);
	        stage.setScene(scene);
		} catch (IOException e) {
			System.out.println("Error while changing view : " + e.getStackTrace());
		}
	}
	
	@FXML
    private void gotoSignup(ActionEvent event) {
	    gotoView(event, "../view/SignupView.fxml");
    }
    
    @FXML
    private void gotoLogin(ActionEvent event) {
    	gotoView(event, "../view/LoginView.fxml");
    }
    
    @FXML
    private void sendLoginReq(ActionEvent event) {
		int response = requestHandler.login(tfUsername.getText(), pfPassword.getText());
		if (response == 200) {
			requestHandler.getUserID(MainClass.getUser().getPseudo());
			MainClass.setNeedRefresh(true);
			gotoView(event, "../view/MainView.fxml");
		}
		tfUsername.clear();
		pfPassword.clear();
    }
    
    @FXML
    private void sendSignupReq(ActionEvent event) {
		int response = requestHandler.createAccount(tfEmail.getText(), tfUsername.getText(), pfPassword.getText(), tfName.getText(), tfFirstName.getText());
		if (response == 200) { gotoView(event, "../view/LoginView.fxml"); }
    }
    
}
