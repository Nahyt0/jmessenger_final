package fr.jmessenger.client.controller;

import java.io.IOException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.jmessenger.client.MainClass;
import fr.jmessenger.client.model.AddFriend;
import fr.jmessenger.client.model.Room;
import fr.jmessenger.client.model.RoomElement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class RequestHandler {
	
	private String BASE_URI;
	private Client client;
	private ObjectMapper mapper = new ObjectMapper();
	
	public RequestHandler() {
		this.client = ClientBuilder.newClient();
		this.BASE_URI = MainClass.getURI();
	}
	
	// Generic function to POST json to an API
	private Response postRequest(Object obj, String api) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.disable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
		String jsonStr = "";
		try {
			jsonStr = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		Response response = null;
		try {
			response = this.client
					.target(this.BASE_URI + api)
					.request(MediaType.APPLICATION_JSON)
					.post(Entity.json(jsonStr));
		} catch (Exception e) {
			System.out.println("Host not found");
		}
		return response;
	}
	
	/*************************************************
	 *************** USER API COMMANDS ***************
	 *************************************************/
		
	public int createAccount(String email, String pseudo, String password, String nom, String prenom) {
		// send json to BASE_URI + user/create_account

		ObjectNode user = mapper.createObjectNode();
		user.put("email", email);
		user.put("pseudo", pseudo);
		user.put("password", password);
		user.put("nom", nom);
		user.put("prenom", prenom);
		
		Response response = postRequest(user, "user/create_account");

		if (response == null){
			return 503;
		} else if (response.getStatus() == 200) {
			// In case of successful account creation, save the user data in an object
			MainClass.getUser().setEmail(email);
			MainClass.getUser().setPseudo(pseudo);
			MainClass.getUser().setNom(nom);
			MainClass.getUser().setPrenom(prenom);
			System.out.println("Account has been created and linked to this email : " + email);
		} else {
			System.out.println(response.getStatusInfo());
		}
		return response.getStatus();
	}
	
	public int login(String pseudo, String password) {
		// send json to BASE_URI + user/login
		
		ObjectNode login = mapper.createObjectNode();
		login.put("pseudo", pseudo);
		login.put("password", password);

		Response response = postRequest(login, "user/login");
		
		if (response == null){
			return 503;
		} else if (response.getStatus() == 200) {
			// In case of successful login, save the accesstoken and pseudo in an object
			String responseStr = response.readEntity(String.class);
			JSONObject jObject = new JSONObject(responseStr);
			MainClass.getUser().setAccessToken(jObject.get("ACCESS_TOKEN").toString());
			MainClass.getUser().setPseudo(pseudo);
			System.out.println("Successfully logged as " + pseudo);
		} else {
			System.out.println(response.getEntity());
		}
		return response.getStatus();
	}

	public int logout(String pseudo) {
		// send json to BASE_URI + user/logout
		
		ObjectNode logout = mapper.createObjectNode();
		logout.put("pseudo", pseudo);

		Response response = postRequest(logout, "user/logout");
		
		if (response == null){
			return 503;
		} else if (response.getStatus() == 200) {
			System.out.println(pseudo +" has been disconnected");
		} else {
			System.out.println(response.getEntity());
		}
		return response.getStatus();
	}
	
	public int addFriend(String pseudoAmi, String pseudo) {
		// send json to BASE_URI + user/addFriend/{pseudoAmi}
		AddFriend addFriend = new AddFriend(pseudo);

		Response response = postRequest(addFriend, "user/addFriend/" + pseudoAmi);

		if (response == null){
			return 503;
		} else if (response.getStatus() == 200) {
			System.out.println(pseudoAmi + " has been added to your friendlist");
		} else {
			System.out.println(response.getEntity());
		}
		return response.getStatus();
	}
	
	public int getUser(String pseudo) {
		// send json to BASE_URI + user/getUser/{pseudo}
		Response response = this.client
				.target(this.BASE_URI + "user/getUser/" + pseudo)
				.request(MediaType.APPLICATION_JSON)
				.get();
		
		if (response == null){
			return 503;
		} else if (response.getStatus() == 200) {
			String responseStr = response.readEntity(String.class);
			JSONObject jObject = new JSONObject(responseStr);
			MainClass.getUser().setNom(jObject.getString("nom"));
			MainClass.getUser().setPrenom(jObject.getString("prenom"));
			MainClass.getUser().setEmail(jObject.getString("email"));
		} else {
			System.out.println(response.getEntity());
		}
		return response.getStatus();
	}
	
	public int getUserID(String pseudo) {
		// send json to BASE_URI + user/getUser/{pseudo}
		Response response = this.client
				.target(this.BASE_URI + "user/getUser/" + pseudo)
				.request(MediaType.APPLICATION_JSON)
				.get();
		
		if (response == null){
			return 503;
		} else if (response.getStatus() == 200) {
			// In case of successful request, save the id in an object
			String responseStr = response.readEntity(String.class);
			JSONObject jObject = new JSONObject(responseStr);
			MainClass.getUser().setId(jObject.getInt("id"));
		} else {
			System.out.println(response.getEntity());
		}
		return response.getStatus();
	}
	
	/****************************************************
	 **************** MESSAGE API COMMANDS **************
	 ****************************************************/
		
	public int sendMessage(int sender, String content, int room) {
		// send json to BASE_URI + message/send_message
		ObjectNode message = mapper.createObjectNode();
		message.put("sender", sender);
		message.put("content", content);
		message.put("room", room);
		
		Response response = postRequest(message, "message/send_message");
		
		if (response == null){
			return 503;
		} else if (response.getStatus() == 200) {
			System.out.println("Sent");
		} else {
			System.out.println(response.getEntity());
		}
		return response.getStatus();
	}
	
	public Response syncMessages(int roomID) {
		// send GET request to BASE_URI + message/{roomID}/sync
		Response response = this.client
				.target(this.BASE_URI + "message/"+roomID+"/"+MainClass.getLastMessageID()+"/sync")
				.request(MediaType.APPLICATION_JSON)
				.get();
		return response;
	}
	
	/************************************************
	*************** ROOM API COMMANDS ***************
	*************************************************/
	
	public int createRoom(String nom, String createur) {
		// send json to BASE_URI + room/create_room
		Room room = new Room(nom, createur);
		
		Response response = postRequest(room, "room/create_room");
		
		if (response == null){
			return 503;
		} else if (response.getStatus() == 200) {
			String responseStr = response.readEntity(String.class);
			if (!responseStr.equals("")) {
				JSONObject jObject = new JSONObject(responseStr);
				return jObject.getInt("ROOMID");
			}
			System.out.println("Room \""+nom+"\" has been created");
		} else {
			System.out.println(response.getEntity());
		}
		return response.getStatus();
	}
	
	public ObservableList<RoomElement> listRoom(String pseudo) throws JsonProcessingException, IOException {
		// send json to BASE_URI + room/getRoomList
		ObservableList<RoomElement> roomList = FXCollections.observableArrayList();
		ObjectNode usernameNode = mapper.createObjectNode();
		usernameNode.put("pseudo", pseudo);
		Response response = postRequest(usernameNode, "room/getRoomList");
		if (response == null){
			return null;
		} else if (response.getStatus() == 200) {
			String responseStr = response.readEntity(String.class);
			if (!responseStr.equals("")) {			
				ObjectMapper mapper = new ObjectMapper();
				mapper.disable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
				ArrayNode arrayNode;
				arrayNode = (ArrayNode) mapper.readTree(responseStr);
				for (JsonNode subNode : arrayNode) {
					RoomElement room = new RoomElement(subNode.get("ROOMID").asInt(), subNode.get("NAME").asText());
					roomList.add(room);
				}
			}
		} else {
			System.out.println(response.getEntity());
			return null;
		}
		return roomList;
	}
	
	public int joinRoom(int roomID, String pseudo) {
		// send json to BASE_URI + room/{roomID}/join
		ObjectNode join = mapper.createObjectNode();
		join.put("pseudo", pseudo);

		Response response = postRequest(join, "room/"+roomID+"/join");
		
		if (response == null){
			return 503;
		} else if (response.getStatus() == 200) {
			System.out.println(pseudo +" has been added to the room number " + roomID);
		} else {
			System.out.println(response.getEntity());
		}
		return response.getStatus();
	}
}
