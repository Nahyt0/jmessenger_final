package fr.jmessenger.client.controller;

/***************************************************************************
 * MainController handle all the interactions between the user and the GUI
 * It allows the user to send messages to different rooms and to add friends
 ***************************************************************************/

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.jmessenger.client.MainClass;
import fr.jmessenger.client.model.RoomElement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class MainController implements Initializable {
	
	private Thread t;
	private RequestHandler requestHandler = new RequestHandler();
	private ObservableList<RoomElement> roomList;
	private int selectedRoomID = 0;
	
    @FXML
    private Label lName;

    @FXML
    private Label lFirstname;

	@FXML
    private Label lUsername;

    @FXML
    private Button btnLogout;

    @FXML
    private Button btnSearchUser;

    @FXML
    private TextField tfSearchUser;

    @FXML
    private Label lUserSearchRes;

    @FXML
    private Button btnAddFriend;

    @FXML
    private Button btnRefreshRoom;

    @FXML
    private ListView<RoomElement> lvRoomsPrivate;
    
    @FXML
    private ListView<RoomElement> lvRoomsPublic;

    @FXML
    private Button btnRefreshPublicRooms;

    @FXML
    private Button btnCreateRoom;

    @FXML
    private TextField tfCreateRoom;

    @FXML
    private TextArea taMessageBox;

    @FXML
    private TextArea taSenderMessage;

    @FXML
    private Button btnSendMessage;

    @FXML
    private Label lRoomName;

    @FXML
    private Button btnJoinRoom;
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// Initialize listView so the user can only select one room at a time and refresh the room list
		lvRoomsPrivate.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		refreshRoomList();
		// Get ans show user informations
		requestHandler.getUser(MainClass.getUser().getPseudo());
		lFirstname.setText(MainClass.getUser().getPrenom());
		lName.setText(MainClass.getUser().getNom());
		lUsername.setText(MainClass.getUser().getPseudo());
		// Hide room information as none is joined
		lRoomName.setVisible(false);
    	btnJoinRoom.setVisible(false);
	}
	
	private void loadRoomMessages() {
		// Create a new task to fetch messages
		SyncThread syncT = new SyncThread(selectedRoomID);
		t = new Thread() {
			public void run() {
				while(MainClass.getNeedRefresh()) {
					try {
						// Get all the messages from the last message
						MainClass.setLastMessageID(lvRoomsPrivate.getSelectionModel().getSelectedItem().getLastMessageID());
						lvRoomsPrivate.getSelectionModel().getSelectedItem().appendContent(syncT.call());
						// Actualize the text area only if the content has changed
						if (!lvRoomsPrivate.getSelectionModel().getSelectedItem().getContent().equals(taMessageBox.getText())) {
							taMessageBox.setText(lvRoomsPrivate.getSelectionModel().getSelectedItem().getContent());
						}
						// Sets the lastMessageID of the RoomElement to avoid getting all the messages each time we change room
						lvRoomsPrivate.getSelectionModel().getSelectedItem().setLastMessageID(MainClass.getLastMessageID());
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						// Syncronize messages each seconds
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		t.start();
	}
	
	private void refreshRoomList() {
		// Clear the room list and fill it again
		lvRoomsPrivate.getItems().clear();
		try {
			roomList = requestHandler.listRoom(MainClass.getUser().getPseudo());
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < roomList.size(); i++) {
			lvRoomsPrivate.getItems().add(roomList.get(i));
		}
	}
	
	// Changes the view with another fxml file
	private void gotoView(ActionEvent event, String fxmlPath) {
		Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource(fxmlPath));
			Node node = (Node) event.getSource();
		    Stage stage = (Stage) node.getScene().getWindow();
	        Scene scene=new Scene(root);
	        stage.setScene(scene);
		} catch (IOException e) {
			System.out.println("Error while changing view : " + e.getStackTrace());
		}
	}
	
	@FXML
    void addFriend(ActionEvent event) {
		// Send friend request to the server
		String friendName = lUserSearchRes.getText();
		String username = MainClass.getUser().getPseudo();
		int roomID;
		int res = requestHandler.addFriend(friendName, username);
		if (res == 200) {
			// Create a room for the 2 new friends
			roomID = requestHandler.createRoom(friendName+"_"+username, username);
			// Both join it
			requestHandler.joinRoom(roomID, username);
			requestHandler.joinRoom(roomID, friendName);
		}
		refreshRoomList();
		lUserSearchRes.setVisible(false);
		btnAddFriend.setVisible(false);
    }

    @FXML
    private void sendMessage(ActionEvent event) {
    	if (selectedRoomID != 0) {
	    	int sender = MainClass.getUser().getId();
	    	requestHandler.sendMessage(sender, taSenderMessage.getText(), selectedRoomID);
    	}
    	taSenderMessage.clear();
    }
    
    @FXML
    private void logout(ActionEvent event) {
    	MainClass.setNeedRefresh(false);
    	requestHandler.logout(MainClass.getUser().getPseudo());
    	gotoView(event, "../view/LoginView.fxml");
    }
    
    @FXML
    private void searchUser(ActionEvent event) {
    	String pseudo = tfSearchUser.getText();
    	int res = requestHandler.getUser(pseudo);
    	if (res == 200) {
    		lUserSearchRes.setText(pseudo);
    		lUserSearchRes.setVisible(true);
    		btnAddFriend.setVisible(true);
    	}
    	tfSearchUser.clear();
    }
    
    @FXML
    void refreshRooms(ActionEvent event) {
    	refreshRoomList();
    }
    
    @FXML
    void joinSelectedRoom(MouseEvent event) throws InterruptedException {
    	if (lvRoomsPrivate.getSelectionModel().getSelectedItem() != null) {
    		// Ask the syncThread to stops and wait until it's done
        	if (t != null) {
        		while (t.isAlive()) {
            		MainClass.setNeedRefresh(false);
            		TimeUnit.MILLISECONDS.sleep(50);
            	}
        	}
        	MainClass.setNeedRefresh(true);
        	// Get the roomID selected and activate the sync of its messages
        	System.out.println("Room selected : " + lvRoomsPrivate.getSelectionModel().getSelectedItem());
        	selectedRoomID = lvRoomsPrivate.getSelectionModel().getSelectedItem().getRoomID();
        	loadRoomMessages();
        	lRoomName.setText( lvRoomsPrivate.getSelectionModel().getSelectedItem().getRoomName());
        	lRoomName.setVisible(true);
        	btnJoinRoom.setVisible(true);	
    	}
    }
    
    @FXML
    void createPublicRoom(ActionEvent event) {

    }
    
    @FXML
    void refreshPublicRooms(ActionEvent event) {

    }
    
}
