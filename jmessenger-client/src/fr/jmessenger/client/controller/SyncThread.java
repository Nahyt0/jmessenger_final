package fr.jmessenger.client.controller;

import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import fr.jmessenger.client.MainClass;
import javafx.concurrent.Task;


public class SyncThread extends Task<String> {
	
	private int roomID;
	private RequestHandler requestHandler = new RequestHandler();
	
	public SyncThread(int roomID) {
		this.roomID = roomID;
	}
	
    public String call() throws Exception {
		String allMessages = "";
		// Ask the server for new messages
		Response response = requestHandler.syncMessages(roomID);
		String responseStr = response.readEntity(String.class);
		if (!responseStr.equals("")) {			
			ObjectMapper mapper = new ObjectMapper();
			// Disables alphabetically sorting to get the messages ordered by id and not by content
			mapper.disable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
			ArrayNode arrayNode = (ArrayNode) mapper.readTree(responseStr);
			for (JsonNode subNode : arrayNode) {
				// Creates formatted string for the output : "sender : content"
				String sender = subNode.get("SENDER").asText();
				String content = subNode.get("CONTENT").asText();
				MainClass.setLastMessageID(subNode.get("MESSAGEID").asInt());
				// Append to the whole messages string
				allMessages += sender + " : " + content + "\n";
			}
		}
    	return allMessages;
    }
}
