package fr.jmessenger.client;

import java.util.List;
import java.util.Scanner;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.jmessenger.client.controller.RequestHandler;
import fr.jmessenger.client.model.User;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.*;

public class MainClass extends Application {
	
	private static String BASE_URI;
	private static int lastMessageID = 0;
	private static Boolean needRefresh = true;
	private static User user = null;
	

	@Override
	public void start(Stage primaryStage) {
		// Manage parameters
		Parameters params = getParameters();
		List<String> list = params.getRaw();
		if (list.size() >= 2) {
			for (int i = 0; i < list.size(); i+=2) {
				if (list.get(i).equals("--host") && list.get(i+1).equals("") == false){
					BASE_URI = list.get(i+1);
				} else if (list.get(i).equals("--cmdline")) {
					user = new User();
					commandLineClient("http://localhost:80/", user);
					System.exit(0);
				}
			}
		} else {
			BASE_URI = "http://localhost:80/";
		}
		// Creates an empty user
		setUser(new User());
		
		// Prepare the scene for the UI
		Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource("view/LoginView.fxml"));
			primaryStage.setTitle("JMessenger");
			primaryStage.setScene(new Scene(root));
			primaryStage.show();
		} catch (Exception e) {
			System.out.println("Error creating the main stage : " + e.getStackTrace());
		}
	}
	
	private void commandLineClient(String BASE_URI, User user) {
		String userInput = "";
		String[] commands;
		RequestHandler requestHandler = new RequestHandler();
		while(userInput.contains("/exit") != true)
        {
            Scanner input = new Scanner(System.in);
            userInput = input.nextLine();
			if (userInput.startsWith("/")) {
				commands = userInput.split(" ", -1);
				switch (commands[0]) {
					case "/login":
						if (commands.length == 3 ) {
							requestHandler.login(commands[1], commands[2]);
						} else {
							System.out.println("Wrong usage\nUse the commands /login as following : /login username");
						}
						break;
					case "/logout":
						if (commands.length == 2 ) {
							requestHandler.logout(commands[1]);
						} else {
							System.out.println("Wrong usage\nUse the commands /logout as following : /logout username");
						}
						break;
					case "/create_account":
						if (commands.length == 6 ) {
							requestHandler.createAccount(commands[1], commands[2], commands[3], commands[4], commands[5]);
						} else {
							System.out.println("Wrong usage\nUse the commands /create_account as following : /create_account email, username, password, name, firstname");
						}
						break;
					case "/add_friend":
						if (commands.length == 2 ) {
							requestHandler.addFriend(commands[1], user.getPseudo());
						} else {
							System.out.println("Wrong usage\nUse the commands /add_friend as following : /add_friend friendID");
						}
						break;
					case "/create_room":
						if (commands.length == 2 ) {
							requestHandler.createRoom(commands[1], user.getPseudo());
						} else {
							System.out.println("Wrong usage\nUse the commands /create_room as following : /create_room roomName");
						}
						break;
					case "/join_room":
						if (commands.length == 2 ) {
							requestHandler.joinRoom(Integer.parseInt(commands[1]), user.getPseudo());
						} else {
							System.out.println("Wrong usage\nUse the commands /join_room as following : /join_room roomID");
						}
						break;
					case "/exit":
						System.out.println("Closing client...");
						input.close();
						break;
					default:
						System.out.println("Unknown command");
						break;
				}
			} else {
				// get current room
				// get userID
				requestHandler.sendMessage(4, userInput, 1);
			}	
        }
	}
	
	@Override
	public void stop() {
    	// Stops syncThread and logout before exit
		setNeedRefresh(false);

		// send json to BASE_URI + user/logout
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode logout = mapper.createObjectNode();
		logout.put("pseudo", user.getPseudo());
		String jsonStr = logout.toString();
		Client client = ClientBuilder.newClient();
		client
			.target(BASE_URI + "user/logout")
			.request(MediaType.APPLICATION_JSON)
			.post(Entity.json(jsonStr));
	}
	
	// ************ GETTERS AND SETTERS ***************
	
	public static void main(String[] args) {
		launch(args);
	}
	
	public static Boolean getNeedRefresh() {
		return needRefresh;
	}

	public static void setNeedRefresh(Boolean needRefresh) {
		MainClass.needRefresh = needRefresh;
	}
	
	public static User getUser() {
		return user;
	}
	
	public static void setUser(User _user) {
		user = _user;
	}
	
	public static String getURI() {
		return BASE_URI;
	}
	
	public static int getLastMessageID() {
		return lastMessageID;
	}

	public static void setLastMessageID(int lastMessageID) {
		MainClass.lastMessageID = lastMessageID;
	}

	
}
