package serveur.jmessenger.fr;

public class Message {

	private int sender;
	private String content;
	private int room;
	private int messageid;
	
	
	public int getSender() {
		return sender;
	}
	
	public void setSender(int sender) {
		this.sender = sender;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}

	public int getRoom() {
		return room;
	}

	public void setRoom(int room) {
		this.room = room;
	}

	public int getMessageid() {
		return messageid;
	}
	
}
