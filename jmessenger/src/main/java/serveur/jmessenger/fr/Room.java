package serveur.jmessenger.fr;

public class Room {
	
	private int id;
	private String nom;
	private String createur;
	
	public Room() {}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCreateur() {
		return createur;
	}

	public void setCreateur(String createur) {
		this.createur = createur;
	}

	public int getId() {
		return id;
	}
}
