package serveur.jmessenger.fr;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

@Path("/room")
public class RoomController {
	
	private Database db = new Database();
	private ObjectMapper mapper = new ObjectMapper();
	
	public RoomController(){}
	
	@POST
	@Path("/create_room")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createRoom(Room room) {
		db.start();
		String createRoom = "";
		String content;
		ObjectNode infoNode = mapper.createObjectNode();
		int roomID = 0;
		
		try {
			
			Connection c = db.getConnection();
			try {
				PreparedStatement ps = null;
			
				createRoom = "INSERT INTO Room(NAME, CREATOR) VALUES (?, ?);";

				String getUser = "SELECT ACCESSTOKEN, USERID FROM User WHERE PSEUDO = ?;";
				
				ps = c.prepareStatement(getUser);
				
				ps.setString(1, room.getCreateur());
				
				ResultSet rsCreateur = ps.executeQuery();
				
				int userID = 0;
				
				if (rsCreateur.next()) {
					if (rsCreateur.getString(1) == null) {
						db.stop();
						return Response.status(400).entity("{\n\t\"ERROR\":\"ACCESS_TOKEN_NOT_FOUND\"\n}").build();
					}
					else
						userID = rsCreateur.getInt(2);
				}else {
					db.stop();
					return Response.status(400).entity("{\n\t\"ERROR\":\"CREATOR_NOT_FOUND\"\n}").build();
				
				}
				
				ps = c.prepareStatement(createRoom);
				ps.setString(1, room.getNom());
				ps.setString(2, room.getCreateur());
			
				ps.executeUpdate();
				
				String getRoom = "SELECT ROOMID FROM Room WHERE NAME = ?;";
				
				ps = c.prepareStatement(getRoom);
				
				ps.setString(1, room.getNom());
				
				ResultSet rsRoom = ps.executeQuery();
				
				if (rsRoom.next()) {
					roomID = rsRoom.getInt(1);
				} else {
					db.stop();
					return Response.status(400).entity("{\n\t\"ERROR\":\"ROOM_NOT_FOUND\"\n}").build();
				}
				
				String joinRoom = "INSERT INTO UserRoom(ROOMID, USERID) VALUES (?, ?);";

				ps = c.prepareStatement(joinRoom);
	
				ps.setInt(1, roomID);
				ps.setInt(2, userID);
	
				ps.executeUpdate();
				
				ps.close();
				
				infoNode.put("ROOMID", roomID);
				infoNode.put("NAME", room.getNom());
				content = new String(infoNode.toString());
				
			} catch (SQLException e) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				db.stop();
				return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			}
			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			db.stop();
			return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			
		}

		db.stop();
		
		return Response.status(200).entity(content).build();
	}
	
	@POST
	@Path("/getRoom")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRoomID(Room room) {
		db.start();
		int roomID = 0;
		String content;
		ObjectNode infoNode = mapper.createObjectNode();
		try {
			
			Connection c = db.getConnection();
			try {
				PreparedStatement ps = null;
				String getRoom = "SELECT ROOMID FROM Room WHERE NAME = ?;";
				
				ps = c.prepareStatement(getRoom);
				
				ps.setString(1, room.getNom());
				
				ResultSet rsRoom = ps.executeQuery();
				
				if (rsRoom.next()) {
					roomID = rsRoom.getInt(1);
				} else {
					db.stop();
					return Response.status(400).entity("{\n\t\"ERROR\":\"ROOM_NOT_FOUND\"\n}").build();
				}
				
				ps.close();
				
				infoNode.put("ROOMID", roomID);
				content = new String(infoNode.toString());
				
			} catch (SQLException e) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				db.stop();
				return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			}
			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			db.stop();
			return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			
		}

		db.stop();
		
		return Response.status(200).entity(content).build();
	}
	
	@POST
	@Path("/getRoomList")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRoomList(User user) {
		db.start();
		
		String allContent;
		
		try {
			
			Connection c = db.getConnection();
			try {
				PreparedStatement ps = null;
				String getUserID = "SELECT USERID, ACCESSTOKEN FROM User WHERE PSEUDO = ?;";
				String getRoomList = "SELECT ROOMID FROM UserRoom WHERE USERID = ?;";
				
				ps = c.prepareStatement(getUserID);
				
				ps.setString(1, user.getPseudo());
				ResultSet rsFriend = ps.executeQuery();
				int userID = 0;
				    	
				if (rsFriend.next()) {
					if (rsFriend.getString(2) == "") {
						db.stop();
						return Response.status(400).entity("{\n\t\"ERROR\":\"ACCESS_TOKEN_NOT_FOUND\"\n}").build();
					}else
						userID = rsFriend.getInt(1);
					
				} else {
					db.stop();
					return Response.status(400).entity("{\n\t\"ERROR\":\"USER_NOT_FOUND\"\n}").build();
				}
				
				ps = c.prepareStatement(getRoomList);
				ps.setInt(1, userID);
				
				ResultSet rsRoom = ps.executeQuery();
				ArrayNode roomArrayNode = mapper.createArrayNode();
				while (rsRoom.next()) {
					ObjectNode infoNode = mapper.createObjectNode();
				
					String getNameRoom = "SELECT NAME FROM Room WHERE ROOMID = ?;";
					ps = c.prepareStatement(getNameRoom);
				
				
					ps.setInt(1, rsRoom.getInt(1));
					ResultSet rsRoomName = ps.executeQuery();
		
					
					if (rsRoomName.next()) {
						infoNode.put("ROOMID", rsRoom.getInt(1));
						infoNode.put("NAME", rsRoomName.getString(1));
						
					} else {
						db.stop();
						return Response.status(400).entity("{\n\t\"ERROR\":\"ROOM_NOT_FOUND\"\n}").build();
					}
					roomArrayNode.add(infoNode);
				}
				
				ps.close();
				
				allContent = new String(roomArrayNode.toString());
				
			} catch (SQLException e) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				db.stop();
				return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			}
			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			db.stop();
			return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			
		}

		db.stop();
		
		return Response.status(200).entity(allContent).build();
	}
	
	@POST
	@Path("/{roomID}/join")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response join(User user,@PathParam("roomID") int roomID) {
		db.start();
		
		try {
			
			Connection c = db.getConnection();
			try {
					int userID = 0;
					String getuserID = "SELECT USERID FROM User WHERE PSEUDO = ?;";
					
					PreparedStatement psUserID = c.prepareStatement(getuserID);
					
					psUserID.setString(1, user.getPseudo());
					ResultSet rsuserID = psUserID.executeQuery();
					
					if(rsuserID.next()) {
						userID = rsuserID.getInt(1);
					}
					
					String checkAlreadyJoin = "SELECT USERID FROM UserRoom WHERE ROOMID = ?;";
					
					PreparedStatement psAlreadyJoin = c.prepareStatement(checkAlreadyJoin);
					
					psAlreadyJoin.setInt(1, roomID);
					ResultSet rsAlreadyJoin = psAlreadyJoin.executeQuery();
	
					boolean lock = true;
					
					while(rsAlreadyJoin.next()) {
						if(userID == rsAlreadyJoin.getInt(1))
							lock = false;
					}
					
					if (lock) {
						String createRoom = "INSERT INTO UserRoom(ROOMID, USERID) VALUES (?, ?);";

						PreparedStatement ps = c.prepareStatement(createRoom);
			
						ps.setInt(1, roomID);
						ps.setInt(2, userID);
			
						ps.executeUpdate();
					
					}else {
						db.stop();
						return Response.status(400).entity("{\n\t\"ERROR\":\"USER_ALREADY_IN_ROOM\"\n}").build();
					}
				
				
				
			} catch (SQLException e) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				db.stop();
				return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			}
			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			db.stop();
			return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			
		}

		db.stop();
		
		return Response.status(200).entity("{}").build();
	}
}
