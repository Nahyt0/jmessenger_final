package serveur.jmessenger.fr;

import java.sql.*;

public class Database {
	private Connection c = null;
	
	public Database() {}
	
	public void start() {
	      try {
	         Class.forName("org.sqlite.JDBC");
	         c = DriverManager.getConnection("jdbc:sqlite:jmessenger.db");
	      
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	         System.exit(0);
	      }
	      System.out.println("Opened database successfully");
	}
	
	public void stop() {
	      try {     
	         c.close();
	      
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	         System.exit(0);
	      }
	      System.out.println("Closed database successfully");
	}
	
	public void createTables() {
		try {
			Statement stmt = c.createStatement();
			
			String createUser = "CREATE TABLE IF NOT EXISTS User " +
                    "(USERID             INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " EMAIL              TEXT, " + 
                    " PSEUDO             TEXT     NOT NULL, " + 
                    " PWD                CHAR(128) NOT NULL," +
                    " NOM                TEXT, " + 
                    " PRENOM             TEXT,"  +
                    " ACCESSTOKEN        TEXT)"; 
			stmt.executeUpdate(createUser);
			
			String createRoom = "CREATE TABLE IF NOT EXISTS Room " +
                    "(ROOMID              INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " NAME                TEXT	   NOT NULL, " + 
                    " CREATOR             TEXT     NOT NULL)"; 
			stmt.executeUpdate(createRoom);
			
			String createMessage = "CREATE TABLE IF NOT EXISTS Message " +
                    "(MESSAGEID          INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " CONTENT            TEXT     NOT NULL, " +
                    " USERID             INT      NOT NULL," +
                    " ROOMID             INT      NOT NULL," +
                    " FOREIGN KEY (USERID) REFERENCES User(USERID)," + 
                    " FOREIGN KEY (ROOMID) REFERENCES Room(ROOMID))"; 
			stmt.executeUpdate(createMessage);
			
			String createFriend = "CREATE TABLE IF NOT EXISTS Friend "
					+ "(USERID			 INT	  NOT NULL,"
					+ "FRIENDID			 INT	  NOT NULL,"
					+ "FOREIGN KEY (USERID) REFERENCES User(USERID),"
					+ "FOREIGN KEY (FRIENDID) REFERENCES User(USERID));";
			stmt.executeUpdate(createFriend);
			
			String createUserInRoom = "CREATE TABLE IF NOT EXISTS UserRoom" + 
					"(ROOMID		 INT	  NOT NULL," + 
					"USERID			 INT	  NOT NULL," + 
					"FOREIGN KEY (ROOMID) REFERENCES Room(ROOMID)," + 
					"FOREIGN KEY (USERID) REFERENCES User(USERID));";
			stmt.executeUpdate(createUserInRoom);
			
			stmt.close();
			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Table created successfully");
		stop();
	}
	
	public Connection getConnection() {
		return this.c;
	}
}