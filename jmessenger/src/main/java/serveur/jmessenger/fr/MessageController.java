package serveur.jmessenger.fr;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Path("/message")
public class MessageController {
		
		private Database db = new Database();
		private ObjectMapper mapper = new ObjectMapper();
		
		public MessageController(){}
		
		@POST
		@Path("/send_message")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response sendMessage(Message message) {
			db.start();
			String createMessage = "";
			String content;
			ObjectNode infoNode = mapper.createObjectNode();
			
			try {
				
				Connection c = db.getConnection();
				try {
					PreparedStatement ps = null;
				
					createMessage = "INSERT INTO Message(CONTENT, USERID, ROOMID) VALUES (?, ?, ?);";
					
					ps = c.prepareStatement(createMessage);
					ps.setString(1, message.getContent());
					ps.setInt(2, message.getSender());
					ps.setInt(3, message.getRoom());
					
					ps.executeUpdate();
					
					String getPseudo = "SELECT PSEUDO, ACCESSTOKEN FROM User WHERE USERID = ?;";
					
					ps = c.prepareStatement(getPseudo);
					
					ps.setInt(1, message.getSender());
					
					ResultSet rsPseudo = ps.executeQuery();
				
					if (rsPseudo.next()) {
						if (rsPseudo.getString(2) == "") {
							db.stop();
							return Response.status(400).entity("{\n\t\"ERROR\":\"ACCESS_TOKEN_NOT_FOUND\"\n}").build();
						}
						infoNode.put("MESSAGEID", message.getMessageid());
						infoNode.put("SENDER", message.getSender());
						infoNode.put("CONTENT", message.getContent());
						
					} else {
						db.stop();
						return Response.status(400).entity("{\n\t\"ERROR\":\"SENDER_NOT_FOUND\"\n}").build();
					}
					content = new String(infoNode.toString());
					ps.close();
					
				} catch (SQLException e) {
					System.err.println( e.getClass().getName() + ": " + e.getMessage() );
					db.stop();
					return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\"\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
				}
				
			} catch ( Exception e ) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				db.stop();
				return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\"\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
				
			}

			db.stop();
			
			return Response.status(200).entity(content).build();
		}
		
		@GET
		@Path("/{roomID}/{messageid}/sync")
		@Produces(MediaType.APPLICATION_JSON)
		public Response sync(@PathParam("roomID") int roomID, @PathParam("messageid") int messageid) {
			db.start();
			String selectMessages = "";
			String allContent = "";
			
			try {
				
				Connection c = db.getConnection();
				try {
					PreparedStatement ps = null;
				
					selectMessages = "SELECT MESSAGEID, USERID, CONTENT FROM Message WHERE ROOMID = ? AND MESSAGEID > ? ORDER BY MESSAGEID ASC;";
					
					ps = c.prepareStatement(selectMessages);
					ps.setInt(1, roomID);
					ps.setInt(2, messageid);
					String getPseudo = "";
					
					ResultSet rs = ps.executeQuery();
					
					ArrayNode messageArrayNode = mapper.createArrayNode();
					
					while(rs.next()) {	

						getPseudo = "SELECT pseudo FROM User WHERE USERID = ?;";
						
						ps = c.prepareStatement(getPseudo);
						
						ps.setInt(1, rs.getInt(2));
						
						ObjectNode infoNode = mapper.createObjectNode();
						
						ResultSet rsPseudo = ps.executeQuery();
						
						if (rsPseudo.next()) {
							infoNode.put("MESSAGEID", rs.getInt(1));
							infoNode.put("SENDER", rsPseudo.getString(1));
							infoNode.put("CONTENT", rs.getString(3));
					
						} else {
							db.stop();
							return Response.status(400).entity("{\n\t\"ERROR\":\"SENDER_NOT_FOUND\"\n}").build();
						}
						
						messageArrayNode.add(infoNode);
					}
					
					allContent = new String(messageArrayNode.toString());
					
					ps.close();
					
				} catch (SQLException e) {
					System.err.println( e.getClass().getName() + ": " + e.getMessage() );
					db.stop();
					return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
				}
				
			} catch ( Exception e ) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				db.stop();
				return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
				
			}

			db.stop();
			
			return Response.status(200).entity(allContent).build();
		}
}
