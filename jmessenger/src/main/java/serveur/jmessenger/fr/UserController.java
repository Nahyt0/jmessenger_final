package serveur.jmessenger.fr;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Path("/user")
public class UserController {
	private Database db = new Database();

	private ObjectMapper mapper = new ObjectMapper();
	
	public UserController(){ }

	@POST
	@Path("/create_account")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createUser(User user) {
		db.start();
		String createAccount;
		
		try {
			
			Connection c = db.getConnection();
			try {
				PreparedStatement ps = null;
				
				if( user.getEmail() == null) {
					createAccount = "INSERT INTO User(PSEUDO, PWD, NOM, PRENOM) VALUES (?, ?, ?, ?);";
					
					ps = c.prepareStatement(createAccount);
					
					ps.setString(1, user.getPseudo());
					ps.setString(2, User.getSHA(user.getPassword()));
					ps.setString(3, user.getNom());
					ps.setString(4, user.getPrenom());
					
				}else {
					createAccount = "INSERT INTO User(EMAIL, PSEUDO, PWD, NOM, PRENOM) VALUES (?, ?, ?, ?, ?);";

					ps = c.prepareStatement(createAccount);
					
					ps.setString(1, user.getEmail());
					ps.setString(2, user.getPseudo());
					ps.setString(3, User.getSHA(user.getPassword()));
					ps.setString(4, user.getNom());
					ps.setString(5, user.getPrenom());
					
				}
				ps.executeUpdate();
				ps.close();
				
			} catch (SQLException e) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				db.stop();
				return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			}
			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			db.stop();
			return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			
		}
		
		db.stop();
		
		return Response.status(200).entity("{}").build();
	}
	
	@GET
	@Path("/getUser/{pseudo}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@PathParam("pseudo") String pseudo) {
		
		User user = new User();
		
		db.start();
		
		try {
			
			Connection c = db.getConnection();
			String getUser = "SELECT * FROM User WHERE PSEUDO = ?;";
			
			try {
				PreparedStatement ps = c.prepareStatement(getUser);
				ps.setString(1, pseudo);
				ResultSet rs = ps.executeQuery();
	            
				while (rs.next()) {
	                user.setEmail(rs.getString(2));
	                user.setPseudo(rs.getString(3));
	                user.setPassword(rs.getString(4));
	                user.setNom(rs.getString(5));
	                user.setPrenom(rs.getString(6));
	                user.setId(rs.getInt(1));
	                
	            }
	            rs.close();
				ps.close();
			
			} catch (SQLException e) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				db.stop();
				return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			}
			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			db.stop();
			return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();			
		}
		
		db.stop();
		
		if ( user.getPassword() == null ) {
			db.stop();
			return Response.status(404).entity("{\n\t\"ERROR\":\"USER_NOT_FOUND\"\n}").build();
		}else
			return Response.status(200).entity(user).build();
	}
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(User user) {
		db.start();
		int userID = 0;
		
		try {
			
			Connection c = db.getConnection();
			try {
				PreparedStatement ps = null;
				
				//################################################################################################
				
				String loginRequest = "SELECT PWD, USERID FROM User WHERE pseudo = ?";
					
				ps = c.prepareStatement(loginRequest);
					
				ps.setString(1, user.getPseudo());

				ResultSet rs = ps.executeQuery();
				
				if(rs.next()) {
					if(!rs.getString(1).equals(User.getSHA(user.getPassword()))) {
						return Response.status(403).entity("{\n\t\"ERROR\":\"INVALID_PASSWORD\"\n}").build();
						
					}
				}else {
					db.stop();
					return Response.status(404).entity("{\n\t\"ERROR\":\"USER_NOT_FOUND\"\n}").build();
				}
				
				rs.close();
				
				//################################################################################################
				
				String getUser = "SELECT USERID FROM User WHERE PSEUDO = ?;";
				
				ps = c.prepareStatement(getUser);
				
				ps.setString(1, user.getPseudo());
				ResultSet rsUser = ps.executeQuery();
				    	
				if (rsUser.next()) {
					userID = rsUser.getInt(1);
					
				} else {
					db.stop();
					return Response.status(400).entity("{\n\t\"ERROR\":\"USER_NOT_FOUND\"\n}").build();
				}
				
				//#################################################################################################
			
				String inputAccessToken = "UPDATE User SET ACCESSTOKEN = ? WHERE USERID = ?;";
				
				ps = c.prepareStatement(inputAccessToken);
				
				ps.setString(1, user.getAccessToken(user.getPseudo(), userID));
				ps.setInt(2, userID);
				
				ps.executeUpdate();
				
				ps.close();
				
			} catch (SQLException e) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				db.stop();
				return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			}
			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			db.stop();
			return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			
		}
		
		db.stop();
		
		return Response.status(200).entity("{\n\t\"ACCESS_TOKEN\":\"" + user.getAccessToken(user.getPseudo(), userID) + "\"\n}").build();
	}
	
	@POST
	@Path("/logout")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response logout(User user) {
		db.start();
		
		try {
			
			Connection c = db.getConnection();
			try {
				PreparedStatement ps = null;
				
				//################################################################################################
				
				String getUser = "SELECT USERID FROM User WHERE PSEUDO = ?;";
				
				ps = c.prepareStatement(getUser);
				
				ps.setString(1, user.getPseudo());
				ResultSet rsUser = ps.executeQuery();
				
				int userID = 0;
				    	
				if (rsUser.next()) {
					userID = rsUser.getInt(1);
					
				} else {
					db.stop();
					return Response.status(400).entity("{\n\t\"ERROR\":\"USER_NOT_FOUND\"\n}").build();
				}
				
				//#################################################################################################
			
				String inputAccessToken = "UPDATE User SET ACCESSTOKEN = ? WHERE USERID = ?;";
				
				ps = c.prepareStatement(inputAccessToken);
				
				ps.setString(1, null);
				ps.setInt(2, userID);
				
				ps.executeUpdate();
				
				ps.close();
				
			} catch (SQLException e) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				db.stop();
				return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			}
			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			db.stop();
			return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			
		}
		
		db.stop();
		
		return Response.status(200).entity("{}").build();
	}
	
	@POST
	@Path("/reset_password")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response resetPwd(User user) {
		db.start();
		
		try {
			
			Connection c = db.getConnection();
			
			try {
				String resetPwdRequest = "UPDATE User SET PWD = ? WHERE PSEUDO = ?;";
				
				PreparedStatement ps = c.prepareStatement(resetPwdRequest);
					
				ps.setString(1, User.getSHA(user.getPassword()));
				ps.setString(2, user.getPseudo());

				ps.executeUpdate();
		
				ps.close();
				
			} catch (SQLException e) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				db.stop();
				return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			}
			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			db.stop();
			return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			
		}
		
		db.stop();
		
		return Response.status(200).entity("{}").build();
	}
	
	@POST
	@Path("/addFriend/{pseudo}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addFriend(User user, @PathParam("pseudo") String pseudo) {
		db.start();
		
		try {
			
			Connection c = db.getConnection();
			String getUser = "SELECT USERID, ACCESSTOKEN FROM User WHERE PSEUDO = ?;";
			String addFriendRequest = "INSERT INTO Friend(USERID, FRIENDID) VALUES (?, ?);";
			
			try {
				PreparedStatement ps = c.prepareStatement(getUser);
				
				ps.setString(1, pseudo);
				ResultSet rsFriend = ps.executeQuery();
				int friendID = 0;
				    	
				if (rsFriend.next()) {
					friendID = rsFriend.getInt(1);
					
				} else {
					db.stop();
					return Response.status(400).entity("{\n\t\"ERROR\":\"USER_NOT_FOUND\"\n}").build();
				}
				
				ps.setString(1, user.getPseudo());
				ResultSet rs = ps.executeQuery();
				
				int userID = 0;		
					
				if(rs != null) {    	
					while (rs.next()) {
						if (rs.getString(2) == "") {
							db.stop();
							return Response.status(400).entity("{\n\t\"ERROR\":\"ACCESS_TOKEN_NOT_FOUND\"\n}").build();
						} else
							userID = rs.getInt(1);
					}
				} else {
					return Response.status(400).entity("{\n\t\"ERROR\":\"USER_NOT_FOUND\"\n}").build();
				}
				
				ps = c.prepareStatement(addFriendRequest);
				
				ps.setInt(1, userID);
				ps.setInt(2, friendID);

				ps.executeUpdate();
		
				ps.close();
				
			} catch (SQLException e) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				db.stop();
				return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			}
			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			db.stop();
			return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			
		}
		
		db.stop();
		
		return Response.status(200).entity("{}").build();
	}
	
	@POST
	@Path("/getFriends")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getFriendList(User user) {
		db.start();
		String allContent;
		
		try {
			
			Connection c = db.getConnection();
			String getUser = "SELECT USERID, ACCESSTOKEN FROM User WHERE PSEUDO = ?;";
			String getFriendRequest = "SELECT FRIENDID FROM Friend WHERE USERID = ?;";
			
			try {
				PreparedStatement ps = c.prepareStatement(getUser);
				
				ps.setString(1, user.getPseudo());
				ResultSet rsFriend = ps.executeQuery();
				int friendID = 0;
				    	
				if (rsFriend.next()) {
					friendID = rsFriend.getInt(1);
					if (rsFriend.getString(2) == "") {
						db.stop();
						return Response.status(400).entity("{\n\t\"ERROR\":\"ACCESS_TOKEN_NOT_FOUND\"\n}").build();
					}
				} else {
					db.stop();
					return Response.status(400).entity("{\n\t\"ERROR\":\"USER_NOT_FOUND\"\n}").build();
				}
				
				ps = c.prepareStatement(getFriendRequest);
				ps.setInt(1, friendID);

				ArrayNode messageArrayNode = mapper.createArrayNode();
				
				ResultSet rsListFriend = ps.executeQuery();
				
				while (rsListFriend.next()) {
					
					String getFriendPseudo = "SELECT USERID, PSEUDO FROM User WHERE USERID = ?;";
					
					ps = c.prepareStatement(getFriendPseudo);
					ps.setInt(1, rsListFriend.getInt(1));
					
					ObjectNode infoNode = mapper.createObjectNode();
					
					ResultSet rsFriendPseudo = ps.executeQuery();
					
					if(rsFriendPseudo.next()) {
						infoNode.put("USERID", rsFriendPseudo.getInt(1));
						infoNode.put("PSEUDO", rsFriendPseudo.getString(2));
					
					}else {
						db.stop();
						return Response.status(500).entity("{\n\t\"ERROR\":\"NO_FRIENDS\"\n}").build();
					}
					messageArrayNode.add(infoNode);
				}
				allContent = new String(messageArrayNode.toString());
				
				ps.close();
				
			} catch (SQLException e) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				db.stop();
				return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			}
			
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			db.stop();
			return Response.status(500).entity("{\n\t\"ERROR\":\"INTERNAL_ERROR\",\n\t\"" + e.getClass().getName() + "\": \"" + e.getMessage() + "\"\n}").build();
			
		}
		
		db.stop();
		
		return Response.status(200).entity(allContent).build();
		
	}
}