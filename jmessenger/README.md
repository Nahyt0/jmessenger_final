# Jmessenger

## Mode d'emploi

### Description

Jmessenger est une messagerie (presque) instantanée, basée sur la méthode REST.
Le projet est complètement écrit en Java et le serveur n'utilise que les FrameWorks _glassfish_ (pour le serveur) et _jax-rs_ ( pour tous ce qui est REST).
Le client lui utilise _Java FX_ (pour tous ce qui est graphique). Le serveur ainsi que le client se base sur le modèle MVC. 
L'application propose différentes fonctionnalités:
* Créer un compte
* Se connecter
* Se déconnecter
* Créer une room
* Ajouter un ami
* Inviter un ou plusieurs amis dans une room
* Envoyer un message
* Choisir parmi la liste de room où l'utilisateur est


### Prérequis

* Maven (3.5.0): Pour le bon fonctionnement du serveur il est recommendé d'avoir Maven d'installer pour que toutes les dépendences utiles au projet soient installées.
* Java JDK/JRE 1.8

### Tutoriel

Il existe sur ce projet deux dossier étant eux-mêmes deux projet git distinct ( Serveur: https://gitlab.com/Nahyt0/jmessenger.git, Client: <urlGit> ). 
Pour exécuter chaque projet, il faut se trouver dans le dossier où le _pom.xml_ se trouve. Ensuite lancer les commandes suivantes : _mvn package_ et _mvn exec:java_. 
Il y a alors côté serveur ce message qui apparait dans la console:

![alt text](img/server.png)

Et côté client, cette fenêtre qui apparait: 

![alt text](img/client.png)

## Diagramme de Classes

Serveur :
![alt text](img/diagramme-serveur.png)

Client :
![alt text](img/Diagramme-Client.png)